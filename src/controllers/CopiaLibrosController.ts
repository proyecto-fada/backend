import Utils from "../utils/Utils";
import { ILibro } from "../interfaces/ILibro";
import { join } from "path/posix";



class CopiaLibrosController {
  private problemData: string;
  private problemDataArray: string[];
  private libros: ILibro[];
  private cantidadEscritores: number;
  private cantidadLibros: number;
  private solutionFileData: string;

  constructor() {
    this.problemData = "";
    this.problemDataArray = [];
    this.libros = [];
    this.cantidadEscritores = 0;
    this.cantidadLibros = 0;
    this.solutionFileData = "";
  }

  getSolutionFileData(): string {
    return this.solutionFileData;
  }

  //*********************************SE INICIALIZA LA INFORMACION DEL PROBLEMA**************************/
  public InitializeProblemData(fileData: any): void {
    try {
      this.problemData = Utils.ReadFileData(fileData);
      let objCadenaDividida: any = Utils.DividirCadena(
        this.problemData,
        "",
        true
      );
      if (!objCadenaDividida.status) {
        throw new Error("Error al dividir la cadena, verifique el archivo");
      }
      this.problemDataArray = objCadenaDividida.cadena;
      this.ObtenerOtrosDatos();
      this.ObtenerLibros();
    } catch (error) {
      throw error;
    }
  }

  private ObtenerLibros(): void {
    let libro: ILibro;
    try {
      for (let i = 1; i < this.problemDataArray.length; i++) {
        let linea: string = this.problemDataArray[i];

        let objCadenaDividida: any = Utils.DividirCadena(linea, ",");
        if (!objCadenaDividida.status) {
          throw new Error(
            "Error al obtener el nombre del libro y sus paginas, verifique el archivo"
          );
        }
        let arrayDeCadenas: string[] = objCadenaDividida.cadena;
        libro = {
          nombre: arrayDeCadenas[0],
          paginas: parseInt(arrayDeCadenas[1]),
        };
        this.libros.push(libro);
      }
    } catch (error) {
      throw error;
    }
  }

  private ObtenerOtrosDatos(): void {
    try {
      let linea: string = this.problemDataArray[0];
      let objCadenaDividida: any = Utils.DividirCadena(linea, ",");
      if (!objCadenaDividida.status) {
        throw new Error(
          "Error al obtener la cantidad de escritores y la cantidad de libros, verifique el archivo"
        );
      }
      let arrayDeCadenas: string[] = objCadenaDividida.cadena;

      this.cantidadEscritores = parseInt(arrayDeCadenas[0]);
      this.cantidadLibros = parseInt(arrayDeCadenas[1]);
    } catch (error) {
      throw error;
    }
  }
  //*********************************SE INICIALIZA LA INFORMACION DEL PROBLEMA**************************/

  //*********************************CODIGO SOLUCION RECURSIVA******************************************/

  /**
   * ***********************************************************************************************
   * *****************************************ALGORITMO*********************************************
   * ***********************************************************************************************
   * Ordena los números en orden descendente
   * Luego, suma iterativamente el siguiente número de paginas más grande
   * a un conjunto en el que la suma actual sea la más pequeña, obteniendo ese conjunto
   * de ordenar ascendentemente el arreglo de subsets en su propiedad sum
   * 
   * ***********************************************************************************************
   * **********************************COMPLEJIDAD DEL TIEMPO***************************************
   * ***********************************************************************************************
   * 1.Estamos inicializando el arreglo de salida segun la cantidad de escritores, eso nos toma un tiempo de Θ(n)
   * 2.Estamos ordenando el erreglo, eso nos toma un tiempo de O(n log n) en el peor de los casos.
   * 3.Se esta iterando sobre el arreglo de libros, eso nos toma un tiempo de Θ(n).
   * 4.En cada iteracion ordenamos el arreglo de subsets, eso nos toma un tiempo de O(n log n) en el peor de los casos.
   * 
   * En total, eso nos toma un tiempo de Θ(n) + O(n log n) + O(n(n log n)).
   * 
   * Si contamos el tiempo que nos toma recorrer el arreglo solucion debemos agregar un tiempo de Θ(n*m), siendo n la cantidad 
   * de subsets(arreglos) obtenidos y m la cantidad de libros dentro de cada subset
   * Siendo; Θ(n) + O(n log n) + O(n(n log n)) + Θ(n*m)
   */
  private greedyPartition(libros: ILibro[], escritores: number): any[] {
    try {
      let totalPaginas: number = 0;

      libros.sort((a, b) => b.paginas - a.paginas);

      const out: any[] = [...Array(escritores)].map((x) => {
        return {
          sum: 0,
          libros: [],
        };
      });

      for (const elem of libros) {
        const chosenSubset = out.sort((a, b) => a.sum - b.sum)[0];
        chosenSubset.libros.push(elem);
        chosenSubset.sum += elem.paginas;
        totalPaginas += elem.paginas;
      }

      return [out, totalPaginas];

    } catch (error) {
      throw error;
    }
  }

  private GenerarSolucionKPartition(arr: any[], totalPaginas: number): void {
    try {
      let indiceInicial: number = 0;
      let indiceFinal: number = 0;

      for (let i = 0; i < arr.length; i++) {
        this.solutionFileData += `Escritor ${i + 1}:\n`;

        if(i===0){
          indiceInicial = 1;
          indiceFinal = arr[i].libros.length;
        }else{
          indiceInicial = indiceFinal + 1;
          indiceFinal += arr[i].libros.length;
        }

        if(arr[i].libros.length === 1){
          this.solutionFileData += `  Se le asigna el libro ${indiceInicial}\n`;
        }else{
          this.solutionFileData += `  Se le asignan los libros del ${indiceInicial} al ${indiceFinal}\n`;
        }

        this.solutionFileData += `Total paginas asignadas: ${arr[i].sum}\n\n`;
      }
      this.solutionFileData = `${totalPaginas}\n` + this.solutionFileData

    } catch (error) {
      throw error;
    }
  }

  private GenerarSolucionGreedyPartition(arr: any[], totalPaginas: number): void {
    try {
      for (let i = 0; i < arr.length; i++) {
        this.solutionFileData += `Escritor ${i + 1}:\n`;
        for (let j = 0; j < arr[i].libros.length; j++) {
          this.solutionFileData += `  ${arr[i].libros[j].nombre} ${arr[i].libros[j].paginas}\n`;
        }
        this.solutionFileData += `Total paginas asignadas: ${arr[i].sum}\n\n`;
      }
      this.solutionFileData = `${totalPaginas}\n` + this.solutionFileData

    } catch (error) {
      throw error;
    }
  }

  public LlamarSolucionGreedyPartition(): void {
    try {
      let t0 = performance.now();
      let solucion: any[] = this.greedyPartition(this.libros, this.cantidadEscritores);
      this.GenerarSolucionGreedyPartition(solucion[0], solucion[1]);
      let t1 = performance.now();
      this.solutionFileData += `\n\nTiempo de ejecución: ${(t1 - t0)} ms\n`;
    } catch (error) {
      throw error;
    }
  }

  public LlamarSolucionBasica(): void {
    try {
      let t0 = performance.now();
      let solucion: any[] = this.KParttion(this.libros, this.cantidadEscritores, this.cantidadLibros);
      this.GenerarSolucionKPartition(solucion[0], solucion[1]);
      let t1 = performance.now();
      this.solutionFileData += `\n\nTiempo de ejecución: ${(t1 - t0)} ms\n`;
    } catch (error) {
      throw error;
    }
  }

  /**
  * ***********************************************************************************************
  * *****************************************ALGORITMO*********************************************
  * ***********************************************************************************************
  * Crea un array de objetos de salida que corresponde a la cantidad de escritores, cada objeto
  * tiene la suma de paginas que lleva el escritor y un arreglo de libros que le corresponden.
  * 
  * Obtenemos el promedio de paginas de cada escritor
  * Obtenemos el promedio de libros de cada escritor
  * 
  * Hacemos un ciclo hasta que ya no hayan mas libros que agregar
  * En cada ciclo preguntamos si el escritor actual no supero el promedio de paginas ni supero el promedio de libros,
  * si no lo supero, agregamos el libro al arreglo de libros del escritor actual y sumamos la paginas del libro agregado
  * a la suma de paginas del escritor actual
  * 
  * En casa contrario, agregamos el libro al arreglo de libros del escritor siguiente 
  * y sumamos la paginas del libro agregado a la suma de paginas del escritor actual.
  * 
  * Definimos un factor fijo de 1.2 para que el promedio de paginas no sea tan bajo, esto para
  * garantizar que el al ultimo escritor no se le asignen una cantidad de libros mayor considerando la cantidad de
  * los escritores anteriores.
  * 
  * ***********************************************************************************************
  * **********************************COMPLEJIDAD DEL TIEMPO***************************************
  * ***********************************************************************************************
  * 1.Estamos inicializando el arreglo de salida segun la cantidad de escritores, eso nos toma un tiempo de Θ(n)
  * 2.Estamos obteniendo el total de paginas por todos los libros, eso nos toma un tiempo de Θ(m)
  * 3.Estamos haciendo un ciclo hasta que ya no hayan mas libros que agregar, eso nos toma un tiempo de Θ(m)
  * 
  * En total, eso nos toma un tiempo de Θ(n) + Θ(m) + Θ(m)
  * 
  * Si contamos el tiempo que nos toma recorrer el arreglo solucion debemos agregar un tiempo de O(n*m), siendo n la cantidad 
  * de subsets(arreglos) obtenidos y m la cantidad de libros dentro de cada subset
  * Siendo; Θ(n) + Θ(m) + Θ(m) + Θ(n*m)
  */

  private KParttion(libros: ILibro[], escritores: number, cantidadLibros: number): any[] {
    try {
      let indiceLibroEscogido: number = 0;
      let totalPaginas: number = 0;
      let paginasPorEscritor: number = 0;
      let librosInsertados: number = 0;
      let indiceEscritor: number = 0;
      const promedioLibrosPorEscritor: number = Math.ceil(cantidadLibros / escritores);
      const factorPromedioPaginas: number = 1.2;

      const solucion: any[] = [...Array(escritores)].map((x) => {
        return {
          sum: 0,
          libros: [],
        };
      });

      for (let index = 0; index < cantidadLibros; index++) {
        totalPaginas += libros[index].paginas;
      }

      paginasPorEscritor = Math.ceil(totalPaginas / escritores);

      while (librosInsertados < cantidadLibros) {

        if (solucion[indiceEscritor].sum + libros[indiceLibroEscogido].paginas <= (paginasPorEscritor * factorPromedioPaginas)) {

          if (solucion[indiceEscritor].libros.length >= promedioLibrosPorEscritor) {
            if (indiceEscritor < escritores - 1) {
              indiceEscritor++;
            }
          }

        } else {

          if (indiceEscritor < escritores - 1) {
            indiceEscritor++;
          }

        }

        solucion[indiceEscritor].libros.push(libros[indiceLibroEscogido]);
        solucion[indiceEscritor].sum += libros[indiceLibroEscogido].paginas;

        librosInsertados++;
        indiceLibroEscogido++;

      }

      return [solucion, totalPaginas];

    } catch (error) {
      throw error;
    }
  }
  //*********************************CODIGO SOLUCION RECURSIVA******************************************/


  //*********************************CODIGO SOLUCION DINAMICA******************************************/
  public LlamarSolucionDinamica(): void {
    try {

    } catch (error) {
      throw error;
    }
  }

  // private UltimoProcedimientoSinConflictoBS(arr: ILibro[], start_index: number) {
  //   let lo: number = 0;
  //   let hi: number = start_index - 1;

  //   while (lo <= hi) {
  //     let mid = Math.floor((lo + hi) / 2);
  //     if (arr[mid].horaFinEnSegundos <= arr[start_index].horaInicioEnSegundos) {
  //       if (
  //         arr[mid + 1].horaFinEnSegundos <=
  //         arr[start_index].horaInicioEnSegundos
  //       )
  //         lo = mid + 1;
  //       else return mid;
  //     } else hi = mid - 1;
  //   }

  //   return -1;
  // }

  //*********************************CODIGO SOLUCION DINAMICA******************************************/

}

export default CopiaLibrosController;